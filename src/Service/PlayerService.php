<?php
namespace App\Service;


use App\Entity\Team;
use App\Entity\Player;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlayerService extends CrudService implements IPlayerService
{


    // ALT+INS, Override, __construct + Implement all
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        parent::__construct($em, $formFactory);
    }

    public function getRepo(): EntityRepository
    {
        return $this->em->getRepository(Player::class);
    }
    public function getAllPlayers(): iterable
    {
        return $this->getRepo()->findAll();
        // WARNING! Query Iterator / Query builder with pagination
    }

    public function getPlayersByTeam(int $teamId): iterable
    {
        return $this->getRepo()->findBy(["player_team"=>$teamId]);
    }

    public function getPlayersByVisibility(bool $isVisible): iterable
    {
        // return $this->getRepo()->findBy(["car_visible"=>$isVisible]);
        $qb = $this->em->createQueryBuilder();
        $qb->select("player")
            // ->addSelect()
            ->from(Player::class, "player")
            // ->innerJoin(Team::class, "team")
            ->where("player.player_visible = :visible")
            ->orderBy("player.rating", "desc")
            // ->addOrderBy()
            ->setParameter("visible", $isVisible);
        $query = $qb->getQuery();
        // $query->setMaxResults()
        return $query->getResult();
        // WARNING! Query Iterator / Query builder with pagination
        // TO READ: conditions as predicates VS conditions as expressions
        // ->expr()->constant(20)->addBigger()->constant(30)
    }

    public function getPlayerById(int $playerId): Player
    {
        /** @var Player|null $onePlayer */
        $onePlayer = $this->getRepo()->find($playerId);
        if ($onePlayer == null){
            throw new NotFoundHttpException("NO CAR FOUND");
        }
        return $onePlayer;
    }

    public function savePlayer(Player $onePlayer): void
    {
        $this->em->persist($onePlayer);
        $this->em->flush();
    }

    public function removePlayer(int $playerId): void
    {
        $onePlayer = $this->getPlayerById($playerId);
        $this->em->remove($onePlayer);
        $this->em->flush();
    }

    public function getPlayerForm(Player $onePlayer): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $onePlayer);
        $form->add("name", TextType::class, [ "required"=>false ]);
       // $form->add("team", TextType::class, [ "required"=>false ]);
        $form->add("team", EntityType::class, [ 'class' => Team::class ]);
        $form->add("country", TextType::class, [ "required"=>false ]);
        $form->add("apps", NumberType::class, [ "required"=>false ]);
        $form->add("mins", NumberType::class, [ "required"=>false ]);
        $form->add("goals", NumberType::class, [ "required"=>false ]);
        $form->add("assists", NumberType::class, [ "required"=>false ]);
        $form->add("ps", NumberType::class, [ "required"=>false ]);
        $form->add("red", NumberType::class, [ "required"=>false ]);
        $form->add("yel", NumberType::class, [ "required"=>false ]);
        $form->add("spg", NumberType::class, [ "required"=>false ]);
        $form->add("rating", NumberType::class, [ "required"=>false ]);



        $form->add("SAVE", SubmitType::class);
        return $form->getForm();
    }
}