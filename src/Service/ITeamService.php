<?php
namespace App\Service;


use App\Entity\Team;
use Symfony\Component\Form\FormInterface;

interface ITeamService
{
    /**
     * @return Team[]|iterable
     */
    public function getAllTeams() : iterable;


    /**
     * @param int $teamId
     * @return Team
     */
    public function getTeamById(int $teamId) : Team;

    /**
     * @param int $teamId
     * @return iterable|Team[]
     */
    public function getTeamsByPlayer(int $playerId) : iterable;


    /**
     * @param bool $isVisible
     * @return iterable|Team[]
     */
    public function getTeamsByVisibility(bool $isVisible) : iterable;




    /**
     * @param Team $oneTeam
     * @return FormInterface
     */
    public function getTeamForm(Team $oneTeam) : FormInterface;


    /**
     * @param Team $oneTeam
     */
    public function saveTeam(Team $oneTeam) : void;


}