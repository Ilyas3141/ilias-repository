<?php
namespace App\Service;


use App\Entity\Brand;
use App\Entity\Car;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CarService extends CrudService implements ICarService
{
    // ALT+INS, Override, __construct + Implement all
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        parent::__construct($em, $formFactory);
    }

    public function getRepo(): EntityRepository
    {
        return $this->em->getRepository(Car::class);
    }
    public function getAllCars(): iterable
    {
        return $this->getRepo()->findAll();
        // WARNING! Query Iterator / Query builder with pagination
    }

    public function getCarsByBrand(int $brandId): iterable
    {
        return $this->getRepo()->findBy(["car_brand"=>$brandId]);
    }

    public function getCarsByVisibility(bool $isVisible): iterable
    {
        // return $this->getRepo()->findBy(["car_visible"=>$isVisible]);
        $qb = $this->em->createQueryBuilder();
        $qb->select("car")
            // ->addSelect()
            ->from(Car::class, "car")
            // ->innerJoin(Brand::class, "brand")
            ->where("car.car_visible = :visible")
            ->orderBy("car.car_price", "desc")
            // ->addOrderBy()
            ->setParameter("visible", $isVisible);
        $query = $qb->getQuery();
        // $query->setMaxResults()
        return $query->getResult();
        // WARNING! Query Iterator / Query builder with pagination
        // TO READ: conditions as predicates VS conditions as expressions
        // ->expr()->constant(20)->addBigger()->constant(30)
    }

    public function getCarById(int $carId): Car
    {
        /** @var Car|null $oneCar */
        $oneCar = $this->getRepo()->find($carId);
        if ($oneCar == null){
            throw new NotFoundHttpException("NO CAR FOUND");
        }
        return $oneCar;
    }

    public function saveCar(Car $oneCar): void
    {
        $this->em->persist($oneCar);
        $this->em->flush();
    }

    public function removeCar(int $carId): void
    {
        $oneCar = $this->getCarById($carId);
        $this->em->remove($oneCar);
        $this->em->flush();
    }

    public function getCarForm(Car $oneCar): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $oneCar);
        $form->add("car_model", TextType::class, [ "required"=>false ]);
        $form->add("car_price", NumberType::class, [ "required"=>false ]);
        $form->add("car_visible", ChoiceType::class, [
           "choices"=>[ "YES" => true, "NO" => false]
        ]); // KEY: displayed to the user VALUE: saved in the entity
        $form->add("car_brand", EntityType::class, [
           "class" => Brand::class,
           "choice_label"=>"brand_name", // Displayed to the user
           "choice_value"=>"brand_id" // PK value saved in the db
            // "query_builder"
        ]);
        $form->add("SAVE", SubmitType::class);
        return $form->getForm();
    }
}