<?php
namespace App\Service;


use App\Entity\Player;
use Symfony\Component\Form\FormInterface;

interface IPlayerService
{
    /**
     * @return Player[]|iterable
     */
    public function getAllPlayers() : iterable;

    /**
     * @param int $teamId   {#$playerId}
     * @return iterable|Player[]
     */
    public function getPlayersByTeam(int $teamId) : iterable;

    /**
     * @param bool $isVisible
     * @return iterable|Player[]
     */
    public function getPlayersByVisibility(bool $isVisible) : iterable;

    /**
     * @param int $plaeyrId
     * @return Player
     */
    public function getPlayerById(int $playerId) : Player;

    /**
     * @param Player $onePlayer
     */
    public function savePlayer(Player $onePlayer) : void;

    /**
     * @param int $playerId
     */
    public function removePlayer(int $playerId) : void;

    /**
     * @param Player $onePlayer
     * @return FormInterface
     */
    public function getPlayerForm(Player $onePlayer) : FormInterface;
}