<?php
namespace App\Service;

use App\Entity\Car;
use Symfony\Component\Form\FormInterface;

interface ICarService
{
    /**
     * @return Car[]|iterable
     */
    public function getAllCars() : iterable;

    /**
     * @param int $brandId
     * @return iterable|Car[]
     */
    public function getCarsByBrand(int $brandId) : iterable;

    /**
     * @param bool $isVisible
     * @return iterable|Car[]
     */
    public function getCarsByVisibility(bool $isVisible) : iterable;

    /**
     * @param int $carId
     * @return Car
     */
    public function getCarById(int $carId) : Car;

    /**
     * @param Car $oneCar
     */
    public function saveCar(Car $oneCar) : void;

    /**
     * @param int $carId
     */
    public function removeCar(int $carId) : void;

    /**
     * @param Car $oneCar
     * @return FormInterface
     */
    public function getCarForm(Car $oneCar) : FormInterface;
}