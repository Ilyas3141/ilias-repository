<?php


namespace App\Service;



use App\Entity\Team;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class TeamService extends CrudService implements ITeamService
{
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        parent::__construct($em, $formFactory);
    }

    public function getRepo(): EntityRepository
    {
        return $this->em->getRepository(Team::class);
    }

    public function getAllTeams(): iterable
    {
        return $this->getRepo()->findAll();
        // WARNING! Query Iterator / Query builder with pagination
    }


    public function getTeamById(int $teamId): Team
    {
        /** @var Team|null $oneTeam */
        $oneTeam = $this->getRepo()->find($teamId);
        if ($oneTeam == null) {
            throw new NotFoundHttpException("NO CAR FOUND");
        }
        return $oneTeam;
    }

    public function getTeamsByVisibility(bool $isVisible): iterable
    {
        // return $this->getRepo()->findBy(["car_visible"=>$isVisible]);
        $qb = $this->em->createQueryBuilder();
        $qb->select("team")
            // ->addSelect()
            ->from(Team::class, "team")
            // ->innerJoin(Team::class, "team")
            ->where("team.team_visible = :visible")
            //  ->orderBy("player.rating", "desc")
            // ->addOrderBy()
            ->setParameter("visible", $isVisible);
        $query = $qb->getQuery();
        // $query->setMaxResults()
        return $query->getResult();
        // WARNING! Query Iterator / Query builder with pagination
        // TO READ: conditions as predicates VS conditions as expressions
        // ->expr()->constant(20)->addBigger()->constant(30)
    }

    public function getTeamsByPlayer(int $playerId): iterable
    {
        return $this->getRepo()->findBy(["teams" => $playerId]);
    }


    public function saveTeam(Team $oneTeam): void
    {
        $this->em->persist($oneTeam);
        $this->em->flush();
    }



    public function getTeamForm(Team $oneTeam): FormInterface
    {
        $form = $this->formFactory->createBuilder(FormType::class, $oneTeam);
        $form->add("team_name", TextType::class, [ "required"=>false ]);
        // $form->add("team", TextType::class, [ "required"=>false ]);
     //   $form->add("team", EntityType::class, [ 'class' => Team::class ]);
        $form->add("gamesplayed", NumberType::class, [ "required"=>false ]);
        $form->add("wins", NumberType::class, [ "required"=>false ]);
        $form->add("draws", NumberType::class, [ "required"=>false ]);
        $form->add("lost", NumberType::class, [ "required"=>false ]);
        $form->add("gf", NumberType::class, [ "required"=>false ]);
        $form->add("ga", NumberType::class, [ "required"=>false ]);
        $form->add("points", NumberType::class, [ "required"=>false ]);

        $form->add("SAVE", SubmitType::class);
        return $form->getForm();
    }


}