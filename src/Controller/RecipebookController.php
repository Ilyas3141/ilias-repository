<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecipebookController extends AbstractController
{
    private $f2name = "../templates/recipes/ingredients.txt\"";
    private $fname = "../templates/recipes/recipes.txt";

    /**
     * @param Request $request
     * @return Response
     * @Route(name="rbList1", path="/rbing")
     */

    public function rbListActionIng(Request $request) : Response
    {
        $twigParams = [ "entries" => array() ];

        if (file_exists($this->f2name))
        {
            $entries = file($this->f2name, FILE_IGNORE_NEW_LINES);
            $entry = ["text"=>""];

            foreach ($entries as $line){
               // $bar='|';
               // $pos=strripos($line, $bar);
              //  $first = substr($line, 0, $pos);
              //  $rest = substr($line, $pos+1);
                // if ($first=='#'){
                if ($entry["text"]) $twigParams["entries"][] = $entry;
                $entry = ["text"=>""];
               // $entry["name"] = $line;
                // $entry = ["name"=>"", "email"=>"", "text"=>""];
                // } else if ($first=="@"){
                //$entry["email"] = $rest;
                //} //else {
                $entry["text"] .= $line . "\n";
            }
            //  }
            if ($entry["text"]) $twigParams["entries"][] = $entry;
        }
        return $this->render("recipes/list2.html.twig", $twigParams);
        // return new Response("Hello, LIST");
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="rbFrom1", path="/rb/ingform")
     */
    public function rbFromActionIng(Request $request) : Response
    {
        return $this->render("recipes/form2.html.twig",
            [ "currentDate" => date("Y.m.d.") ]);
    }
    /**
     * @param Request $request
     * @return Response
     * @Route(name="rbAdd1", path="/rb/ingadd")
     */
    public function rbAddActionIng(Request $request) : Response
    {
        $name = $request->request->get("entry_name");
        // SANITIZE!!!

        if ($name)
        {
            $newentry = "{$name}\n";
            file_put_contents($this->f2name, $newentry, FILE_APPEND);
            $this->addFlash("notice", "ENTRY SAVED");
        } else {
            $this->addFlash("notice", "DATA ERROR");
        }
        return $this->redirectToRoute("rbList1");
        // return new Response("Hello, ADD");
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="rbList", path="/rb")
     */
    public function rbListAction(Request $request) : Response
    {
        $twigParams = [ "entries" => array() ];

        if (file_exists($this->fname))
        {
            $entries = file($this->fname, FILE_IGNORE_NEW_LINES);
            $entry = ["name"=>"", "text"=>""];

            foreach ($entries as $line){
                $bar='|';
                $pos=strripos($line, $bar);
                if(strpos($line, $bar)){
                    $first = substr($line, 0, $pos);
                    // $firstletter=substr($line, 0);
                    $rest = substr($line, $pos+1);
                    // if ($first=='#'){
                    if ($entry["text"]) $twigParams["entries"][] = $entry;
                    $entry = ["name"=>"", "text"=>""];
                    $entry["name"] = $first;
                    //if ($firstletter!='|')$entry["text"] .= $line . "\n";
                    // $entry = ["name"=>"", "email"=>"", "text"=>""];
                    // } else if ($first=="@"){
                    //$entry["email"] = $rest;
                    //} //else {
                    $entry["text"] .= $rest . "\n";
                }else $entry["text"] .= $line . "\n";
            }
          //  }
            if ($entry["text"]) $twigParams["entries"][] = $entry;
        }
        return $this->render("recipes/list1.html.twig", $twigParams);
        // return new Response("Hello, LIST");
    }
    /**
     * @param Request $request
     * @return Response
     * @Route(name="rbAdd", path="/rb/add")
     */
    public function rbAddAction(Request $request) : Response
    {
        $name = $request->request->get("entry_name");
        $email = $request->request->get("entry_number");
        $text = $request->request->get("entry_text");
        // SANITIZE!!!
        $text = str_replace(["#", "@"], "", $text);
        $name = str_replace(["\r", "\n"], "", $name);
        $email = str_replace(["\r", "\n"], "", $email);

        if ($name && $email && $text)
        {
            $newentry = "{$name}|{$text}\n";
            file_put_contents($this->fname, $newentry, FILE_APPEND);
            $this->addFlash("notice", "ENTRY SAVED");
        } else {
            $this->addFlash("notice", "DATA ERROR");
        }
        return $this->redirectToRoute("rbList");
        // return new Response("Hello, ADD");
    }
    /**
     * @param Request $request
     * @return Response
     * @Route(name="rbFrom", path="/rb/form")
     */
    public function rbFromAction(Request $request) : Response
    {
        return $this->render("recipes/form1.html.twig",
            [ "currentDate" => date("Y.m.d.") ]);
    }
}