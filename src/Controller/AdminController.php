<?php


namespace App\Controller;


use App\Entity\Player;
use App\Entity\Team;
use App\Service\IPlayerService;
use App\Service\ITeamService;
use App\Service\TeamService;
use App\DTO\LoginDto;
use App\DTO\RegistrationDto;
use App\Service\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class AdminController
 * @package App\Controller
 * @Route(path="/players/")
 */



class AdminController extends AbstractController
{
    /** @var IPlayerService */
    private $playerService;

    public function __construct(IPlayerService $playerService)
    {
        $this->playerService = $playerService;
    }


    /**
     * @param Request $request
     * @param int $teamId
     * @return Response
     * @Route(name="protected_content", path="/protected")
     */
    public function protectedAction(Request $request, $teamId=0) : Response
    {
        // if ($this->isGranted("ROLE_ADMIN"))
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
        // return new Response("Something TOP SECRET...");
        if ($teamId){
            $players = $this->playerService->getPlayersByTeam($teamId);
        } else {
            $isVisible = $request->query->getBoolean('isVisible');
            if ($isVisible == null){
                $players = $this->playerService->getAllPlayers();
            } else {
                $players = $this->playerService->getPlayersByVisibility($isVisible);
            }
        }

        return $this->render('players/playerslist.html.twig', ["players" => $players]);
    }











}