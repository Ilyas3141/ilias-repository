<?php
namespace App\Controller;

// https://symfony.com/doc/current/security.html
// https://symfony.com/doc/current/security/form_login_setup.html
// https://symfony.com/doc/4.4/security/guard_authentication.html
// https://ourcodeworld.com/articles/read/1057/how-to-implement-your-own-user-authentication-system-in-symfony-4-3-part-1-creating-a-custom-user-class
// Friends Of Symfony
// https://github.com/FriendsOfSymfony/FOSUserBundle

use App\DTO\LoginDto;
use App\DTO\RegistrationDto;
use App\Service\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /** @var SecurityService */
    private $security;
    /** @var FormFactoryInterface */
    private $formFactory;

    public function __construct(SecurityService $securityService, FormFactoryInterface $formFactory)
    {
        $this->security = $securityService;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="app_register", path="/register")
     */
    public function registerAction(Request $request) : Response
    {
        $dto = new RegistrationDto($this->formFactory, $request);
        $form = $dto->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $this->security->registerUser($dto->getEmail(), $dto->getClearPassword(), $dto->getFirstName(), $dto->getLastName());
            $this->addFlash('notice', "{$dto->getEmail()} REGISTERED SUCCESSFULLY");
            return $this->redirectToRoute('playerlistguest');
        }  #app_register
        return $this->render('security/register.html.twig', ['form'=>$form->createView()]);
    }
    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @Route(name="app_login", path="/login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils) : Response
    {
        $dto = new LoginDto($this->formFactory, $request);
        $dto->setUserName($authenticationUtils->getLastUsername());
        return $this->render('security/login.html.twig', [
           'form' => $dto->getForm()->createView(),
            'myUser' => $this->getUser(),
            'authError'=>$authenticationUtils->getLastAuthenticationError()
        ]);
        // NO $dto->getForm()->handleRequest() => Done by the guard authenticator component!!!
        // access User in any other service: add the CORE Security as DI, Security->getUser
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="app_logout", path="/logout")
     */
    public function logoutAction(Request $request) : Response
    {
        // Never actually executed => Logout will be done by the framework!!
    }
    /**
   //  * @param Request $request
   //  * @return Response
    // * @Route(name="protected_content", path="/protected")
     */
  /*  public function protectedAction(Request $request) : Response
    {
        // if ($this->isGranted("ROLE_ADMIN"))
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
       // return new Response("Something TOP SECRET...");
        return $this->render('players/admin.html.twig');
    }
  */
}