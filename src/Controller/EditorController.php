<?php
namespace App\Controller;

use App\DTO\DtoBase;
use App\DTO\LoginDto;
use App\DTO\TextDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EditorController extends AbstractController
{
    private $passFile = "../templates/editor/users.txt";

    private $dataFile = "../templates/editor/data.txt";

    /** @var FormFactoryInterface */
    private $formFactory;
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @Route(name="editor_create", path="/editor/create")
     * @param Request $request
     * @return Response
     */
    public function createPwfileAction(Request $request): Response
    {
        $str = "";
        $str .= "bill\t" . password_hash("billpass", PASSWORD_DEFAULT) . "\n";
        $str .= "joe\t" . password_hash("joepass", PASSWORD_DEFAULT) . "\n";
        $str .= "admin\t" . password_hash("adminpass", PASSWORD_DEFAULT) . "\n";
        file_put_contents($this->passFile, $str);
        return new Response(nl2br($str));
    }


    /**
     * @param Request $request
     * @return Response
     * @Route(name="userAdd", path="/forum/adduser")
     */
    public function addNewUser(Request $request): Response
    {
        $name = $request->request->get("entry_name");
        $pass = $request->request->get("entry_text");

            $newentry = "{$name}\t" .password_hash($pass, PASSWORD_DEFAULT)."\n" ;
            file_put_contents($this->passFile, $newentry, FILE_APPEND);



        return $this->redirectToRoute("editor");

    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="userAdd1", path="/userform")
     */
    public function regisformAction(Request $request) : Response
    {
        return $this->render("forum/registrationform.html.twig",
            [ "currentDate" => date("Y.m.d.") ]);
    }
    /**
     * @param Request $request
     * @return Response
     * @Route(name="changePassword", path="/changepassform")
     */
    public function changepassAction(Request $request) : Response
    {
        return $this->render("forum/changepassform.html.twig",
            [ "currentDate" => date("Y.m.d.") ]);
    }


    /**
     * @param Request $request
     * @return Response
     * @Route(name="changePass", path="/changeform")
     */

        public function changePassword(Request $request): Response
        {
            $name = $request->request->get("entry_name");
            $newpass = $request->request->get("entry_text");

            //I'm sorry I couldn't change password in file

           $xfile = file($this->passFile);
            $newContent = "";
            //$xfile = file_get_contents($this->file);
            // $replacements=array(1=>"\t$newpass");
            // $arr=explode("\t",$xfile);
            // for ($a = 0; $a <= count($xfile); $a++) {
            foreach ($xfile as $line) {
                $arr = explode("\t", $line);
                //  for ($a = 0; $a <= count($arr); $a++) {
                //foreach ($oldLines as $line){
                //     $arr = explode("|" $line);
                //     if ($arr[0]==$inputName){
                //         $newPassword = password_hash(xxxx);
                //         $newContent.="{$arr[0]}|{$newPassword}\n";
                //     } else {
                //         $newContent.=$line."\n";
                //     }
                //}
                //file_put_contents($passwordFile, $newContent);

                if ($name == $arr[0]) {
                    $newPassword = password_hash($newpass, PASSWORD_DEFAULT);
                    $newContent .= "{$arr[0]}\t{$newPassword}\n";
                } else {
                    $newContent .= $line . "";
                }

                file_put_contents($this->passFile, $newContent);
                    //$line="$oldpassname\t$newpass";
                   // $arr[1] = "\t". password_hash($newpass, PASSWORD_DEFAULT)."\n";
                    //$arr[$a + 1] = "\t$newpass";
                   //file_put_contents($this->passFile, $arr, FILE_APPEND);
                    //$basket = array_replace($arr, $replacements);


            }


        return $this->redirectToRoute("editor");
    }

    /**
     * @Route(name="editor_logout", path="/editor/logout")
     * @param Request $request
     * @return Response
     */
    public function logoutAction(Request $request): Response
    {
        // $this->get = get a service from IoC container!
        // Hidden IoC???? Only the session service is allowed this way!!!
        $this->get('session')->clear();
        $this->addFlash("notice", 'LOGGED OUT');
        return $this->redirectToRoute("editor");
    }

    /**
     * @Route(name="editor", path="/editor")
     * @param Request $request
     * @return Response
     */
    public function editorAction(Request $request): Response
    {
        $twigParams = ["filetext" => "", "sessiontext" => "", "form" => null];
        // return $this->render("editor/editor.html.twig", $twigParams);

        $twigParams["sessiontext"] = $this->get('session')->get('customText');
        if (file_exists($this->dataFile)) {
            $twigParams["filetext"] = file_get_contents($this->dataFile);
        }

        $sessionUser = $this->get('session')->get('userName');
        if ($sessionUser) {
            // $dto = new TextDto($this->get("form.factory"), $request); // HIDDEN IOC!!!!
            $dto = new TextDto($this->formFactory, $request); // RequestStack ?
        } else {
            // $dto = new LoginDto($this->get("form.factory"), $request); // HIDDEN IOC!!!!
            $dto = new LoginDto($this->formFactory, $request);
        }
        /** @var DtoBase $dto */
        $form = $dto->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($sessionUser) {
                $this->processTextInput($dto, $form); // ALT+ENTER
            } else {
                $this->processLoginInput($dto);
            }
            return $this->redirectToRoute("editor");
        }
        $twigParams["form"] = $form->createView();
        return $this->render("editor/editor.html.twig", $twigParams);
    }

    private function processTextInput(TextDto $dto, FormInterface $form): void
    {
        $text = $dto->getTextContent();
        if ($form->get("saveToSession")->isClicked()) {
            $this->get('session')->set('customText', $text);
            $this->addFlash('notice', 'SAVED TO SESSION');
        } else {
            file_put_contents($this->dataFile, $text);
            $this->addFlash('notice', 'SAVED TO FILE');
        }
    }

    private function processLoginInput(LoginDto $dto): void
    {
        $uname = $dto->getUserName();
        $upass = $dto->getUserPass();
        $pwfile = file($this->passFile, FILE_IGNORE_NEW_LINES);
        foreach ($pwfile as $line) {
            $arr = explode("\t", $line);
            if ($uname == $arr[0] && password_verify($upass, $arr[1])) {
                $this->get('session')->set('userName', $arr[0]);
                $this->addFlash('notice', 'LOGIN OK');
                return;
            }
        }
        $this->addFlash('notice', 'LOGIN FAILED');
    }
}