<?php


namespace App\Controller;


use App\Entity\Player;
use App\Entity\Team;
use App\Service\IPlayerService;
use App\Service\ITeamService;
use App\Service\TeamService;
use App\DTO\LoginDto;
use App\DTO\RegistrationDto;
use App\Service\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class AdminTeamController
 * @package App\Controller
 * @Route(path="/players/")
 */



class AdminTeamController extends AbstractController
{
    /** @var ITeamService */
    private $teamService;

    public function __construct(ITeamService $teamService)
    {
        $this->teamService = $teamService;
    }


    /**
     * @param Request $request
     * @param int $playerId
     * @return Response
     * @Route(name="protected_content_teams", path="/protectedteams")
     */
    public function protectedAction(Request $request, $playerId=0) : Response
    {
        // if ($this->isGranted("ROLE_ADMIN"))
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
        // return new Response("Something TOP SECRET...");
        if ($playerId){
            $teams = $this->teamService->getTeamsByPlayer($playerId);
        } else {
            $isVisible = $request->query->getBoolean('isVisible');
            if ($isVisible == null){
                $teams = $this->teamService->getAllTeams();
            } else {
                $teams = $this->teamService->getTeamsByVisibility($isVisible);
            }
        }

        return $this->render('players/adminchangetable.html.twig', ["teams" => $teams]);
    }











}