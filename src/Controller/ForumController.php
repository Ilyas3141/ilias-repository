<?php
namespace App\Controller;

use App\DTO\ForumDto;
use App\Model\MessageListModel;
use App\Model\MessageModel;
use App\Model\TopicListModel;
use App\Model\TopicModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ForumController extends AbstractController
{
    /** @var FormFactoryInterface */
    private $formFactory;
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    private function checkLogin() : void
    {
        if (!$this->get('session')->has('userName'))
        {
            throw $this->createAccessDeniedException();
        }
    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     */
    private function dtoToString(ForumDto $dto) : string
    {
        $uname = $this->getUser();
        // $uname = $this->get('session')->get('userName');
        // $now = date("Y-m-d H:i:s");
        $now = (new \DateTime())->format("Y-m-d H:i:s");
        return "{$uname}|{$now}|{$dto->getTextContent()}\n";
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="forum_topiclist", path="/forum/topiclist")
     */
    public function topicListAction(Request $request) : Response
    {
       // $this->checkLogin();
        $fname = "../templates/forum/topics.txt";
        $DTO = new ForumDto($this->formFactory, $request, "topic");
        $form = $DTO->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // TODO: handle linebreaks in the textContent!!! str_replace
            file_put_contents($fname, $DTO->getTextContent()."\n", FILE_APPEND);
            $this->addFlash('notice', 'TOPIC ADDED');
            return $this->redirectToRoute("forum_topiclist");
        }

        $topiclist = array();
        if (file_exists($fname)){
            $lines = file($fname);
            foreach ($lines as $key=>$line){
                $topic = new TopicModel();
                $topic->setId($key)->setName($line);
                $topiclist[] = $topic;
            }
        }

        $model = new TopicListModel();
        $model->setTopicList($topiclist)->setTopicForm($form->createView());
       // return $this->render("forum/topiclist.html.twig");
        return $this->render("forum/topiclist.html.twig", ["model" => $model]);
        //return new Response("Hello 1");
    }

    /**
     * @param Request $request
     * @param int $topic
     * @return Response
     * @Route(name="forum_msglist", path="/forum/messages/{topic}", requirements={ "topic": "\d+" })
     */
    public function msgListAction(Request $request, int $topic) : Response
    {
     //   $this->checkLogin();
        $fname = "../templates/forum/messages_{$topic}.txt";
        $DTO = new ForumDto($this->formFactory, $request, "message");
        $form = $DTO->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            file_put_contents($fname,$this->dtoToString($DTO), FILE_APPEND );
            $this->addFlash('notice', 'MESSAGE ADDED');
            return $this->redirectToRoute("forum_msglist", ["topic"=>$topic] );
        }

        $messages = array();
        if (file_exists($fname)) {
            $lines = file($fname);
            foreach ($lines as $line) {
                // TODO: handle linebreaks in the textContent!!! str_replace
                $data = explode("|", $line);
                $msg = new MessageModel();
                $msg->setUserName($data[0])->setTimestamp($data[1])->setText($data[2]);
                $messages[] = $msg;
            }
        }

        $model = new MessageListModel();
        $model->setMessageList($messages)->setMessageForm($form->createView());
        return $this->render("forum/msglist.html.twig", ["model" => $model]);
        // return new Response("Hello 2");
    }
}