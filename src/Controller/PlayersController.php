<?php
namespace App\Controller;

use App\Entity\Player;
use App\Entity\Team;
use App\Service\IPlayerService;
use App\Service\ITeamService;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PlayersController
 * @package App\Controller
 * @Route(path="/players/")
 */


class PlayersController extends AbstractController
{
    /** @var IPlayerService */
    private $playerService;


    public function __construct(IPlayerService $playerService)
    {
        $this->playerService = $playerService;
    }
    // routes: carlist(brandId?), carshow(carId), cardel(carId), caredit(carId?)

  /*  public function __construct1(ITeamService $teamService)
    {
        $this->teamService = $teamService;
    }
  */


    // /cars/list
    // /cars/list/5  => brandId=5
    // /cars/list?isVisible=false
    /**
     * @param Request $request
     * @param int $teamId
     * @return Response
     * @Route(name="playerlistguest", path="listguest/{teamId}", requirements={ "teamId": "\d+" } )
     */
    public function listAction(Request $request, $teamId = 0) : Response
    {
        // TODO convert db entities into a ViewModels
        if ($teamId){
            $players = $this->playerService->getPlayersByTeam($teamId);
        } else {
            $isVisible = $request->query->getBoolean('isVisible');
            if ($isVisible == null){
                $players = $this->playerService->getAllPlayers();
            } else {
                $players = $this->playerService->getPlayersByVisibility($isVisible);
            }
        }

        return $this->render('players/playerslistguest.html.twig', ["players" => $players]);
    }


    /**
     * @param Request $request
     * @param int $teamId
     * @return Response
     * @Route(name="playerlist", path="list/{teamId}", requirements={ "teamId": "\d+" } )
     */
  /*  public function adminlistAction(Request $request, $teamId = 0) : Response
    {
        // TODO convert db entities into a ViewModels
        if ($teamId){
            $players = $this->playerService->getPlayersByTeam($teamId);
        } else {
            $isVisible = $request->query->getBoolean('isVisible');
            if ($isVisible == null){
                $players = $this->playerService->getAllPlayers();
            } else {
                $players = $this->playerService->getPlayersByVisibility($isVisible);
            }
        }

        return $this->render('players/playerslist.html.twig', ["players" => $players]);
    }
*/


   /**
     * @param Request $request
     * @param int $playerId
     * @return Response
     * @Route(name="playershow", path="show/{playerId}", requirements={ "playerId": "\d+" } )
     */
    public function showAction(Request $request, $playerId) : Response
    {
        // TODO convert db entity into a viewModel
        $onePlayer = $this->playerService->getPlayerById($playerId);
        return $this->render('players/playersshow.html.twig', ["player"=>$onePlayer]);
    }
    /**
     * @param Request $request
     * @param int $playerId
     * @return Response
     * @Route(name="playerdel", path="del/{playerId}", requirements={ "playerId": "\d+" } )
     */
    public function delAction(Request $request, $playerId) : Response
    {
        $this->playerService->removePlayer($playerId);
        $this->addFlash('notice', 'PLAYER REMOVED');
        return $this->redirectToRoute('playerlist');
    }
    /**
     * @param Request $request
     * @param int $playerId
     * @return Response
     * @Route(name="playeredit", path="edit/{playerId}", requirements={ "playerId": "\d+" } )
     */
    public function editAction(Request $request, $playerId = 0) : Response
    {
        // TODO convert db entity into a form DTO
        if ($playerId) {
            $onePlayer = $this->playerService->getPlayerById($playerId);
        } else {
            $onePlayer = new Player();
        }

        $form = $this->playerService->getPlayerForm($onePlayer);
        $form->handleRequest($request); // $_POST => $onePlayer
        if ($form->isSubmitted() && $form->isValid()){
            $this->playerService->savePlayer($onePlayer);
            $this->addFlash('notice', 'PLAYER SAVED');
            return $this->redirectToRoute('protected_content');
        }
        return $this->render('players/playersedit.html.twig', ["form"=>$form->createView()]);
    }







}
/*
 * HOMEWORK: FIND OUT A PROJECT WORK TOPIC
 * - minimum three entities
 * - PLUS user entity
 * - Create the entity classes
 * - Create the data fixtures to load some initial data
 * - Upload your entity classes + data fixture class in one ZIP via Moodle
 */