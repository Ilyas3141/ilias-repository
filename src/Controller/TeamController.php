<?php
namespace App\Controller;

use App\Entity\Player;
use App\Entity\Team;
use App\Service\IPlayerService;
use App\Service\ITeamService;
use App\Service\TeamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;




/**
 * Class TeamController
 * @package App\Controller
 * @Route(path="/teamtable/")
 */

class TeamController extends AbstractController
{
    /** @var ITeamService */
    private $teamService;

    public function __construct(ITeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    /**
     * @param Request $request
     * @param int $playerId
     * @return Response
     * @Route(name="teamtable", path="teams/{playerId}", requirements={ "playerId": "\d+" } )
     */
     public function listTeamAction(Request $request, $playerId = 0) : Response
      {


                //  $teams = $this->teamService->getTeamById($teamId);

         // $isVisible = $request->query->getBoolean('isVisible');

          //    $teams = $this->teamService->getTeamsByVisibility($isVisible);


          if ($playerId){
              $teams = $this->teamService->getTeamsByPlayer($playerId);
          } else {
              $isVisible = $request->query->getBoolean('isVisible');
              if ($isVisible == null){
                  $teams = $this->teamService->getAllTeams();
              } else {
                  $teams = $this->teamService->getTeamsByVisibility($isVisible);
              }
          }


          return $this->render('players/table.html.twig', ["teams" => $teams]);
      }

    /**
     * @param Request $request
     * @param int $teamId
     * @return Response
     * @Route(name="teamedit", path="teamedit/{teamId}", requirements={ "teamId": "\d+" } )
     */

    public function editAction(Request $request, $teamId = 0) : Response
    {
        // TODO convert db entity into a form DTO
        if ($teamId) {
            $oneTeam = $this->teamService->getTeamById($teamId);
        } else {
            $oneTeam = new Team();
        }

        $form = $this->teamService->getTeamForm($oneTeam);
        $form->handleRequest($request); // $_POST => $onePlayer
        if ($form->isSubmitted() && $form->isValid()){
            $this->teamService->saveTeam($oneTeam);
            $this->addFlash('notice', 'TEAM SAVED');
            return $this->redirectToRoute('protected_content_teams');
        }
        return $this->render('players/teamedit.html.twig', ["form"=>$form->createView()]);
    }







}