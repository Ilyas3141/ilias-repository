<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Car;
use App\Entity\Choice;
use App\Entity\Message;
use App\Entity\Player;
use App\Entity\Question;
use App\Entity\Team;
use App\Entity\Topic;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture implements ContainerAwareInterface
{
    /** @var string */
    private $environment; // dev, test
    /** @var EntityManager */
    private $em;
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $kernel = $this->container->get('kernel');
        if ($kernel) $this->environment = $kernel->getEnvironment();
    }


    public function load(ObjectManager $manager)
    {
        echo "HELLO, FIXTURES\n";
        $this->em = $manager;

        $stackLogger = new DebugStack();
        $echoLogger = new EchoSQLLogger();
        $this->em->getConnection()->getConfiguration()->setSQLLogger($stackLogger);

        $question1 = new Question();
        $question1->setQuText("Do you like PHP?");
        $this->em->persist($question1);

        $question2 = new Question();
        $question2->setQuText("What is your favourite color?");
        $this->em->persist($question2);
        $this->em->flush();
        echo "QUESTIONS OK. QUERIES: ".count($stackLogger->queries)."\n";

        $ans1 = new Choice();
        $ans1->setChoVisible(true);
        $ans1->setChoNumvotes(0);
        $ans1->setChoText("YES");
        $ans1->setChoQuestion($question1);
        $this->em->persist($ans1);

        $ans2 = new Choice();
        $ans2->setChoVisible(true);
        $ans2->setChoNumvotes(0);
        $ans2->setChoText("NO");
        $ans2->setChoQuestion($question1);
        $this->em->persist($ans2);

        $ans3 = new Choice();
        $ans3->setChoVisible(true);
        $ans3->setChoNumvotes(0);
        $ans3->setChoText("RED");
        $ans3->setChoQuestion($question2);
        $this->em->persist($ans3);

        $ans4 = new Choice();
        $ans4->setChoVisible(true);
        $ans4->setChoNumvotes(0);
        $ans4->setChoText("WHITE");
        $ans4->setChoQuestion($question2);
        $this->em->persist($ans4);

        $ans5 = new Choice();
        $ans5->setChoVisible(true);
        $ans5->setChoNumvotes(0);
        $ans5->setChoText("GREEN");
        $ans5->setChoQuestion($question2);
        $this->em->persist($ans5);

        $ans6 = new Choice();
        $ans6->setChoVisible(true);
        $ans6->setChoNumvotes(0);
        $ans6->setChoText("PURPLE");
        $ans6->setChoQuestion($question2);
        $this->em->persist($ans6);

        $this->em->flush();
        echo "CHOICES OK. QUERIES: ".count($stackLogger->queries)."\n";
        echo "\n\n";

        $oneChoice = $this->em->getRepository(Choice::class)->findOneBy(["cho_text"=>"NO"]);
        $oneChoiceId = $oneChoice->getChoId();
        echo "CHOICE #{$oneChoiceId} FETCHED\n";
        $oneChoice->setChoNumvotes(42); // PROXY CLASS
        $this->em->persist($oneChoice);
        $this->em->flush();
        // $numVotes=0;
        $numVotes = $this->em->getRepository(Choice::class)->find($oneChoiceId)->getChoNumvotes();
        echo "MOD OK. VOTES: {$numVotes}. QUERIES: ".count($stackLogger->queries)."\n";
        echo "\n\n";

        // IDENTITY MAP
        echo "NUMBER OF CHOICES FOR QUESTION#1\n";
        $questionId = $question1->getQuId();
        echo $question1->getQuChoices()->count()." BEFORE1\n";
        echo $this->em->getRepository(Question::class)->find($questionId)->getQuChoices()->count()." BEFORE2\n";
        $this->em->clear();
        echo $this->em->getRepository(Question::class)->find($questionId)->getQuChoices()->count()." AFTER\n";
        echo "\n\n";

        $oneChoice = $this->em->getRepository(Choice::class)->find($oneChoiceId);
        $this->em->remove($oneChoice);
        $this->em->flush();
        echo "DEL OK. QUERIES: ".count($stackLogger->queries)."\n";
        echo "\n\n";

        $bmw = new Brand();
        $bmw->setBrandName("BMW");
        $this->em->persist($bmw);

        $audi = new  Brand();
        $audi->setBrandName("Audi");
        $this->em->persist($audi);

        $citroen = new Brand();
        $citroen->setBrandName("Citroen");
        $this->em->persist($citroen);
        $this->em->flush();
        echo "BRANDS OK. QUERIES: ".count($stackLogger->queries)."\n";

        $car = new Car();
        $car->setCarBrand($citroen);
        $car->setCarModel("i7");
        $car->setCarPrice(110000);
        $car->setCarVisible(true);
        $this->em->persist($car);
        $this->em->flush();
        echo "CAR OK. QUERIES: ".count($stackLogger->queries)."\n";

        $secondcar = new Car();
        $secondcar->setCarBrand($bmw);
        $secondcar->setCarModel("i8");
        $secondcar->setCarPrice(120000);
        $secondcar->setCarVisible(true);
        $this->em->persist($secondcar);
        $this->em->flush();
        echo "CAR OK. QUERIES: ".count($stackLogger->queries)."\n";

        $thirdcar = new Car();
        $thirdcar->setCarBrand($audi);
        $thirdcar->setCarModel("i9");
        $thirdcar->setCarPrice(150000);
        $thirdcar->setCarVisible(true);
        $this->em->persist($thirdcar);
        $this->em->flush();
        echo "CAR OK. QUERIES: ".count($stackLogger->queries)."\n";


        //project data

        $liv = new Team();
        $liv->setTeamName("Liverpool");
        $liv->setGamesplayed(29);
        $liv->setWins(27);
        $liv->setDraws(1);
        $liv->setLost(1);
        $liv->setGf(66);
        $liv->setGa(21);
        $liv->setPoints(82);
        $this->em->persist($liv);

        $mancity = new  Team();
        $mancity->setTeamName("Manchester City");
        $mancity->setGamesplayed(28);
        $mancity->setWins(18);
        $mancity->setDraws(3);
        $mancity->setLost(7);
        $mancity->setGf(68);
        $mancity->setGa(31);
        $mancity->setPoints(57);
        $this->em->persist($mancity);

        $lc = new Team();
        $lc->setTeamName("Leicester City");
        $lc->setGamesplayed(29);
        $lc->setWins(16);
        $lc->setDraws(5);
        $lc->setLost(8);
        $lc->setGf(58);
        $lc->setGa(28);
        $lc->setPoints(53);
        $this->em->persist($lc);

        $cfc = new Team();
        $cfc->setTeamName("Chelsea");
        $cfc->setGamesplayed(29);
        $cfc->setWins(14);
        $cfc->setDraws(6);
        $cfc->setLost(9);
        $cfc->setGf(51);
        $cfc->setGa(39);
        $cfc->setPoints(48);
        $this->em->persist($cfc);

        $mu = new Team();
        $mu->setTeamName("Manchester United");
        $mu->setGamesplayed(29);
        $mu->setWins(12);
        $mu->setDraws(9);
        $mu->setLost(8);
        $mu->setGf(44);
        $mu->setGa(30);
        $mu->setPoints(45);
        $this->em->persist($mu);

        $wlvs = new Team();
        $wlvs->setTeamName("Wolverhampton");
        $wlvs->setGamesplayed(29);
        $wlvs->setWins(10);
        $wlvs->setDraws(13);
        $wlvs->setLost(6);
        $wlvs->setGf(30);
        $wlvs->setGa(25);
        $wlvs->setPoints(43);
        $this->em->persist($wlvs);

        $shf = new Team();
        $shf->setTeamName("Sheffield United");
        $shf->setGamesplayed(28);
        $shf->setWins(11);
        $shf->setDraws(10);
        $shf->setLost(7);
        $shf->setGf(30);
        $shf->setGa(25);
        $shf->setPoints(43);
        $this->em->persist($shf);

        $tth = new Team();
        $tth->setTeamName("Tottenham");
        $tth->setGamesplayed(29);
        $tth->setWins(11);
        $tth->setDraws(8);
        $tth->setLost(10);
        $tth->setGf(47);
        $tth->setGa(40);
        $tth->setPoints(41);
        $this->em->persist($tth);

        $ars = new Team();
        $ars->setTeamName("Arsenal");
        $ars->setGamesplayed(28);
        $ars->setWins(9);
        $ars->setDraws(13);
        $ars->setLost(6);
        $ars->setGf(40);
        $ars->setGa(36);
        $ars->setPoints(40);
        $this->em->persist($ars);

        $brn = new Team();
        $brn->setTeamName("Burnley");
        $brn->setGamesplayed(29);
        $brn->setWins(11);
        $brn->setDraws(6);
        $brn->setLost(12);
        $brn->setGf(34);
        $brn->setGa(40);
        $brn->setPoints(39);
        $this->em->persist($brn);

        $cp = new Team();
        $cp->setTeamName("Crystal Palace");
        $cp->setGamesplayed(29);
        $cp->setWins(10);
        $cp->setDraws(9);
        $cp->setLost(10);
        $cp->setGf(26);
        $cp->setGa(32);
        $cp->setPoints(39);
        $this->em->persist($cp);

        $ev = new Team();
        $ev->setTeamName("Everton");
        $ev->setGamesplayed(29);
        $ev->setWins(10);
        $ev->setDraws(7);
        $ev->setLost(12);
        $ev->setGf(37);
        $ev->setGa(46);
        $ev->setPoints(37);
        $this->em->persist($ev);

        $nu = new Team();
        $nu->setTeamName("Newcastle United");
        $nu->setGamesplayed(29);
        $nu->setWins(9);
        $nu->setDraws(8);
        $nu->setLost(12);
        $nu->setGf(25);
        $nu->setGa(41);
        $nu->setPoints(35);
        $this->em->persist($nu);

        $sth = new Team();
        $sth->setTeamName("Southampton");
        $sth->setGamesplayed(29);
        $sth->setWins(10);
        $sth->setDraws(4);
        $sth->setLost(15);
        $sth->setGf(35);
        $sth->setGa(52);
        $sth->setPoints(34);
        $this->em->persist($sth);

        $br = new Team();
        $br->setTeamName("Brighton");
        $br->setGamesplayed(29);
        $br->setWins(6);
        $br->setDraws(11);
        $br->setLost(12);
        $br->setGf(32);
        $br->setGa(40);
        $br->setPoints(29);
        $this->em->persist($br);

        $wu = new Team();
        $wu->setTeamName("West Ham");
        $wu->setGamesplayed(29);
        $wu->setWins(7);
        $wu->setDraws(6);
        $wu->setLost(16);
        $wu->setGf(35);
        $wu->setGa(50);
        $wu->setPoints(27);
        $this->em->persist($wu);

        $wt = new Team();
        $wt->setTeamName("Wattford");
        $wt->setGamesplayed(29);
        $wt->setWins(6);
        $wt->setDraws(9);
        $wt->setLost(14);
        $wt->setGf(27);
        $wt->setGa(44);
        $wt->setPoints(27);
        $this->em->persist($wt);

        $brnth = new Team();
        $brnth->setTeamName("Bournemouth");
        $brnth->setGamesplayed(29);
        $brnth->setWins(7);
        $brnth->setDraws(6);
        $brnth->setLost(16);
        $brnth->setGf(29);
        $brnth->setGa(47);
        $brnth->setPoints(27);
        $this->em->persist($brnth);

        $ast = new Team();
        $ast->setTeamName("Aston Villa");
        $ast->setGamesplayed(28);
        $ast->setWins(7);
        $ast->setDraws(4);
        $ast->setLost(17);
        $ast->setGf(34);
        $ast->setGa(56);
        $ast->setPoints(25);
        $this->em->persist($ast);

        $nrw = new Team();
        $nrw->setTeamName("Norwich");
        $nrw->setGamesplayed(29);
        $nrw->setWins(5);
        $nrw->setDraws(6);
        $nrw->setLost(18);
        $nrw->setGf(25);
        $nrw->setGa(52);
        $nrw->setPoints(21);
        $this->em->persist($nrw);


        // $this->em->flush();
        // echo "BRANDS OK. QUERIES: ".count($stackLogger->queries)."\n";

        $player1 = new Player();
        $player1->setPlayerTeam($mancity);
        $player1->setName("Kevin De Bruyne");
        $player1->setCountry("Belgium");
        $player1->setApps(25);
        $player1->setMins(2156);
        $player1->setGoals(8);
        $player1->setAssists(16);
        $player1->setYel(2);
        $player1->setRed(0);
        $player1->setSpg(2.9);
        $player1->setPs(81.7);
        $player1->setRating(7.92);
        $this->em->persist($player1);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


        $player2 = new Player();
        $player2->setPlayerTeam($wlvs);
        $player2->setName("Adama Traore");
        $player2->setCountry("Spain");
        $player2->setApps(22);
        $player2->setMins(2073);
        $player2->setGoals(4);
        $player2->setAssists(7);
        $player2->setYel(0);
        $player2->setRed(0);
        $player2->setSpg(1.2);
        $player2->setPs(73.6);
        $player2->setRating(7.61);
        $this->em->persist($player2);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";

        $player3 = new Player();
        $player3->setPlayerTeam($mancity);
        $player3->setName("Ryad Mahrez");
        $player3->setCountry("Algeria");
        $player3->setApps(15);
        $player3->setMins(1386);
        $player3->setGoals(7);
        $player3->setAssists(8);
        $player3->setYel(0);
        $player3->setRed(0);
        $player3->setSpg(2.3);
        $player3->setPs(89.4);
        $player3->setRating(7.59);
        $this->em->persist($player3);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


        $player4 = new Player();
        $player4->setPlayerTeam($lc);
        $player4->setName("Ricardo Pereira");
        $player4->setCountry("Portugal");
        $player4->setApps(28);
        $player4->setMins(2520);
        $player4->setGoals(3);
        $player4->setAssists(2);
        $player4->setYel(1);
        $player4->setRed(0);
        $player4->setSpg(0.6);
        $player4->setPs(78.9);
        $player4->setRating(7.5);
        $this->em->persist($player4);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


        $player5 = new Player();
        $player5->setPlayerTeam($liv);
        $player5->setName("Sadio Mane");
        $player5->setCountry("Senegal");
        $player5->setApps(24);
        $player5->setMins(2085);
        $player5->setGoals(14);
        $player5->setAssists(7);
        $player5->setYel(2);
        $player5->setRed(0);
        $player5->setSpg(2.3);
        $player5->setPs(81);
        $player5->setRating(7.5);
        $this->em->persist($player5);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";



        $player6 = new Player();
        $player6->setPlayerTeam($liv);
        $player6->setName("Mohamed Salah");
        $player6->setCountry("Egypt");
        $player6->setApps(26);
        $player6->setMins(2250);
        $player6->setGoals(16);
        $player6->setAssists(6);
        $player6->setYel(1);
        $player6->setRed(0);
        $player6->setSpg(3.8);
        $player6->setPs(76.1);
        $player6->setRating(7.45);
        $this->em->persist($player6);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


        $player7 = new Player();
        $player7->setPlayerTeam($liv);
        $player7->setName("Virgil van Dijk");
        $player7->setCountry("Netherlands");
        $player7->setApps(29);
        $player7->setMins(2610);
        $player7->setGoals(4);
        $player7->setAssists(1);
        $player7->setYel(1);
        $player7->setRed(0);
        $player7->setSpg(0.8);
        $player7->setPs(89.2);
        $player7->setRating(7.4);
        $this->em->persist($player7);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";



        $player8 = new Player();
        $player8->setPlayerTeam($lc);
        $player8->setName("Wilfred Ndidi");
        $player8->setCountry("Nigeria");
        $player8->setApps(20);
        $player8->setMins(1892);
        $player8->setGoals(2);
        $player8->setAssists(1);
        $player8->setYel(3);
        $player8->setRed(0);
        $player8->setSpg(0.8);
        $player8->setPs(84.1);
        $player8->setRating(7.36);
        $this->em->persist($player8);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


        $player9 = new Player();
        $player9->setPlayerTeam($lc);
        $player9->setName("James Maddison");
        $player9->setCountry("England");
        $player9->setApps(27);
        $player9->setMins(2404);
        $player9->setGoals(6);
        $player9->setAssists(3);
        $player9->setYel(4);
        $player9->setRed(0);
        $player9->setSpg(2.2);
        $player9->setPs(83);
        $player9->setRating(7.34);
        $this->em->persist($player9);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


        $player10 = new Player();
        $player10->setPlayerTeam($mu);
        $player10->setName("Marcus Rashford");
        $player10->setCountry("England");
        $player10->setApps(22);
        $player10->setMins(1882);
        $player10->setGoals(14);
        $player10->setAssists(4);
        $player10->setYel(2);
        $player10->setRed(0);
        $player10->setSpg(3.4);
        $player10->setPs(76.4);
        $player10->setRating(7.34);
        $this->em->persist($player10);
        $this->em->flush();
        echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";

        /*

                $topic1=new Topic();
                $topic1->setName("Discussion about players");
                $this->em->persist($topic1);
                $this->em->flush();
                echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";



                $topic2=new Topic();
                $topic2->setName("Discussion about club");
                $this->em->persist($topic2);
                $this->em->flush();
                echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";

                $topic3=new Topic();
                $topic3->setName("Discussion about league");
                $this->em->persist($topic3);
                $this->em->flush();
                echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


                $topic4=new Topic();
                $topic4->setName("Discussion about other leagues");
                $this->em->persist($topic4);
                $this->em->flush();
                echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";


                $message1=new Message();
                $message1->setMsgTopic($topic1);
                $message1->setUserName("ilyas");
                $message1->setTimestamp((new \DateTime())->format("Y-m-d H:i:s"));
                $message1->setText("hi");
                $this->em->persist($message1);
                $this->em->flush();
                echo "PLAYER OK. QUERIES: " . count($stackLogger->queries) . "\n";

        */


    }
}
// CRUD
// Create, Read, Update, Delete

/*
php bin/console doctrine:database:create

php bin/console doctrine:schema:drop --force --full-database
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force

php bin/console doctrine:fixtures:load --no-interaction -vvv
 */
