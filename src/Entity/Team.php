<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Team
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="teams")
 */
class Team
{

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $team_id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $team_name;


    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $gamesplayed;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $gf;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $ga;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $wins;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $lost;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $draws;

    /**
     * @return float|null
     */
    public function getGamesplayed(): ?float
    {
        return $this->gamesplayed;
    }

    /**
     * @param float|null $gamesplayed
     */
    public function setGamesplayed(?float $gamesplayed): void
    {
        $this->gamesplayed = $gamesplayed;
    }

    /**
     * @return float|null
     */
    public function getGf(): ?float
    {
        return $this->gf;
    }

    /**
     * @param float|null $gf
     */
    public function setGf(?float $gf): void
    {
        $this->gf = $gf;
    }

    /**
     * @return float|null
     */
    public function getGa(): ?float
    {
        return $this->ga;
    }

    /**
     * @param float|null $ga
     */
    public function setGa(?float $ga): void
    {
        $this->ga = $ga;
    }

    /**
     * @return float|null
     */
    public function getWins(): ?float
    {
        return $this->wins;
    }

    /**
     * @param float|null $wins
     */
    public function setWins(?float $wins): void
    {
        $this->wins = $wins;
    }

    /**
     * @return float|null
     */
    public function getLost(): ?float
    {
        return $this->lost;
    }

    /**
     * @param float|null $lost
     */
    public function setLost(?float $lost): void
    {
        $this->lost = $lost;
    }

    /**
     * @return float|null
     */
    public function getDraws(): ?float
    {
        return $this->draws;
    }

    /**
     * @param float|null $draws
     */
    public function setDraws(?float $draws): void
    {
        $this->draws = $draws;
    }

    /**
     * @return float|null
     */
    public function getPoints(): ?float
    {
        return $this->points;
    }

    /**
     * @param float|null $points
     */
    public function setPoints(?float $points): void
    {
        $this->points = $points;
    }
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $points;

    /**
     * @var ArrayCollection|null
     * @ORM\OneToMany(targetEntity="Player", mappedBy="player_team")
     */
    private $teams;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->team_name;
    }


    /**
     * @return string|null
     */
    public function getTeamName(): ?string
    {
        return $this->team_name;
    }

    /**
     * @param string|null $team_name
     */
    public function setTeamName(?string $team_name): void
    {
        $this->team_name = $team_name;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getTeams(): ?ArrayCollection
    {
        return $this->teams;
    }

    /**
     * @param ArrayCollection|null $teams
     */
    public function setTeams(?ArrayCollection $teams): void
    {
        $this->teams = $teams;
    }

    /**
     * @return int
     */
    public function getTeamId(): int
    {
        return $this->team_id;
    }


}

