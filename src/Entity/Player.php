<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Player
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="players")
 */
class Player
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $player_id;

    public function getPlayerId(): int
    {
        return $this->player_id;
    }

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean", nullable=true)
     */

    private $player_visible;

    /**
     * @return bool|null
     */
    public function getPlayerVisible(): ?bool
    {
        return $this->player_visible;
    }

    /**
     * @param bool|null $player_visible
     */
    public function setPlayerVisible(?bool $player_visible): void
    {
        $this->player_visible = $player_visible;
    }


    /**
     * @var string|null
     * @ORM\Column(type="string", length=100, nullable=true)
     */

    private $name;
    /**
     * @var string|null
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $apps;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $mins;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $goals;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $assists;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $yel;
    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $red;
    /**
     * @var float|null
     * @ORM\Column(type="decimal")
     */
    private $spg;
    /**
     * @var float|null
     * @ORM\Column(type="decimal")
     */
    private $ps;


    /**
     * @var float|null
     * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
     */
    private $rating;


    /**
     * @var Team|null
     * @ORM\JoinColumn(name="player_team", referencedColumnName="team_id")
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="teams")
     */
    private $player_team;

    public function  __toString()
    {
        $teamName = $this->player_team? $this->player_team->getTeamName() : "N.A.";
        return "{$teamName}";
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return float|null
     */
    public function getApps(): ?float
    {
        return $this->apps;
    }

    /**
     * @param float|null $apps
     */
    public function setApps(?float $apps): void
    {
        $this->apps = $apps;
    }

    /**
     * @return float|null
     */
    public function getMins(): ?float
    {
        return $this->mins;
    }

    /**
     * @param float|null $mins
     */
    public function setMins(?float $mins): void
    {
        $this->mins = $mins;
    }

    /**
     * @return float|null
     */
    public function getGoals(): ?float
    {
        return $this->goals;
    }

    /**
     * @param float|null $goals
     */
    public function setGoals(?float $goals): void
    {
        $this->goals = $goals;
    }

    /**
     * @return float|null
     */
    public function getAssists(): ?float
    {
        return $this->assists;
    }

    /**
     * @param float|null $assists
     */
    public function setAssists(?float $assists): void
    {
        $this->assists = $assists;
    }

    /**
     * @return float|null
     */
    public function getYel(): ?float
    {
        return $this->yel;
    }

    /**
     * @param float|null $yel
     */
    public function setYel(?float $yel): void
    {
        $this->yel = $yel;
    }

    /**
     * @return float|null
     */
    public function getRed(): ?float
    {
        return $this->red;
    }

    /**
     * @param float|null $red
     */
    public function setRed(?float $red): void
    {
        $this->red = $red;
    }

    /**
     * @return float|null
     */
    public function getSpg(): ?float
    {
        return $this->spg;
    }

    /**
     * @param float|null $spg
     */
    public function setSpg(?float $spg): void
    {
        $this->spg = $spg;
    }

    /**
     * @return float|null
     */
    public function getPs(): ?float
    {
        return $this->ps;
    }

    /**
     * @param float|null $ps
     */
    public function setPs(?float $ps): void
    {
        $this->ps = $ps;
    }

    /**
     * @return Team|null
     */
    public function getPlayerTeam(): ?Team
    {
        return $this->player_team;
    }

    //new added
    /**
     * @return Team|null
     */
    public function getTeam(): ?Team
    {
        return $this->player_team;
    }

    /**
     * @param Team|null $player_team
     */
    public function setPlayerTeam(?Team $player_team): void
    {
        $this->player_team = $player_team;
    }

    //new added
    /**
     * @param Team|null $player_team
     */
    public function setTeam(?Team $player_team): void
    {
        $this->player_team = $player_team;
    }



    /**
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }

}