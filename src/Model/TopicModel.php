<?php
namespace App\Model;
/*
 * Entity model : data + constraints - for store in a database
 * DTO model: only data - for transfer
 * Viewmodel (MVVM "VM"): data + events - for display in a window
 * MVC model : data + validation rules - for display in a HTML view, or to validate forms
 * Domain model : business logic
 */

class TopicModel
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;

    // ALT+INSERT, Getters and setters, Fluent syntax

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TopicModel
     */
    public function setId(int $id): TopicModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TopicModel
     */
    public function setName(string $name): TopicModel
    {
        $this->name = $name;
        return $this;
    }
}